const express = require("express");
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/home", (req, res) => {
  res.send("Welcome to the homepage");
});

let users = [
  {
    username: "johndoe",
    password: "johndoe1234",
  },
];

app.get("/users", (req, res) => {
  res.send(users);
});

app.delete("/delete-user", (req, res) => {
  delete username;
  res.send(`User ${req.body.username} has been deleted`);
});

app.listen(port);
